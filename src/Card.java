import java.util.Objects;

public class Card {
  private SuitEnum suit;
  private CardEnum value;

  public Card(SuitEnum suit, CardEnum value) {
    this.suit = suit;
    this.value = value;
  }

  public int getValue() {
    return value.getValue();
  }

  @Override
  public String toString() {
    return "{" +
            "suit=" + suit +
            ", value=" + value +
            '}';
  }

}
