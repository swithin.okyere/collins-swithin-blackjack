import java.util.ArrayList;
import java.util.List;

public class Player {
 private String name;
 private List<Card> hand = new ArrayList<>();
 private  int totalValue;

 private Actions actions;


 public Player(String name, List<Card> hand) {
  this.name = name;
  this.hand = hand;
 }

 public Player(String name) {
  this.name = name;
 }

 void  initialAddToHand(Card cardOne, Card cardTwo){
  hand.add(cardOne);
  hand.add(cardTwo);

 }
 void addToHand(Card card){
  hand.add(card);
 }

 void hitMethod(Card card){
  hand.add(card);
 }

 List<Card> stickMethod(List<Card> hand){
  return hand;
 }

 void goBustMethod(){
  // Player is ejected
 }

 private void addPlayerTotalValue(){
 //set the total value
  totalValue =0;
  for (Card card:
       this.hand) {
   totalValue = totalValue + card.getValue();
  }
 }
 public String getName() {
  return name;
 }
 public List<Card> getHand() {
  return hand;
 }

 public int getTotalValue() {
  addPlayerTotalValue();
  return totalValue;
 }

 public Actions getActions() {
  return actions;
 }

 public void setActions(Actions actions) {
  this.actions = actions;
 }
}
