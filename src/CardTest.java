import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {
    @Test
    public void cardGetValueTest(){
        int value = 11;

        Card newCArd = new Card(SuitEnum.CLUB, CardEnum.ACE);
        assertEquals(11, newCArd.getValue());
    }


}