import java.util.*;

public class Deck {
  private Stack<Card> cards = new Stack<>();

  public Deck() {
    this.cards = deckPopulation();
  }

  public Stack<Card> deckPopulation() {
    for (SuitEnum suit :
            SuitEnum.values()) {
      for (CardEnum value :
              CardEnum.values()) {
        cards.push(new Card(suit, value));
//        cards.add(new HashMap<SuitEnum, CardEnum>(){{put(s,c);}});
//        System.out.println(s+"  suite card "+ c + " index "+ i);
      }
    }
//    System.out.println(cards);
//    Collections.shuffle(cards);
//    System.out.println(cards);
//    System.out.println(cards.get(0).getValue());

    return cards;
  }

  public Stack<Card> getCards() {
    return cards;
  }


}
